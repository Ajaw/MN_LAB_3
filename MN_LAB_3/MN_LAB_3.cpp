#include "stdafx.h"
#include <iostream>
#include <math.h>
#include <time.h>
using namespace std;
//jeden z krancow przedzialu aproksymowania
int przedzial = 2;
//rozmiar wektorow
int const n = 51;
//tablica wejscia x
double wekX[n];
//tablica funkcji
double wekY[n];
//stopien wielomianu
int const stopien = 20;
//tablica wspolczynnikow
double wekA[stopien];
//funkcja wypisujaca wektory X oraz Y
void wypisz()
{
	for (int i = 0; i<n; i++)
	{
		cout << "\t" << wekX[i] << "\t\t" << wekY[i] << endl;
	}
}
//funkcja tablicująca wektory X oraz Y na podstawie przedziału i funkcji aproksymowanej
void laduj(int przedzial)
{
	double temp = (double)przedzial;
	double var = 0;
	for (int i = 0; i<n; i++)
	{

		wekX[i] = (-temp + (double)i*var);
		var = (temp / (n-1)) * 2;
		wekY[i] = cos(wekX[i])*sin(3 * wekX[i]);


	}
	//wypisz();
}



//Funkcje obliczajaca dwumian Newtona
double dwumian(int n, int k)
{
	double wynik = 1.0;
	for (int i = 1; i <= k; i++)
		wynik *= (double)(n - i + 1) / i;
	return wynik;
}
//funkcja obliczajaca iloczyn r^[s] wykorzystywany w wielomianach Grama (ponizej)
double iloczynS(int r, int s)

{
	double wynik = 1;
	for (int i = 0; i <= s - 1; i++)
	{
		wynik *= (r - i);
	}
	//wynik *= (r-s+1); //blad byl
	return wynik;
}
//funkcja obliczajaca wielomiany Grama
double Gram(int k, int n, int q)
{
	double wynik = 0, pom;
	for (int s = 0; s <= k; s++)
	{
		//pom = (iloczynS(q, s) / iloczynS(n, s));
		//pom = pow((double)-1, s);
		//pom = dwumian(k, s);
		//pom = dwumian(k + s, s);
		//pom = (double)(pow(-1.0, s)*dwumian(k, s))*dwumian(k + s, s)*(iloczynS(q, s) / iloczynS(n, s));
		wynik += (double)((pow(-1.0, s)*dwumian(k, s))*dwumian(k + s, s))*(iloczynS(q, s) / iloczynS(n, s));
	}
	return wynik;
}
//funkcja obliczajaca wspolczynnik s_j wykorzystywana w funkcji aproksymujacej
double wspS(int n, int j)
{
	double wynik = 0;
	for (int q = 0; q <= n; q++)
	{
		wynik += Gram(j, n, q)*Gram(j, n, q);
	}
	return wynik;
}
//funkcja obliczajaca wspolczynnik c_j wykorzystywana w funkcji aproksymujacej
double wspC(int n, int j)
{
	double wynik = 0;
	for (int q = 0; q <= n; q++)
	{
		wynik += wekY[q] * Gram(j, n, q);
	}
	return wynik;
}
//funkcja obliczajaca aproksymacje zbudowana na wielomianach Grama
double funA(int m, int n, int q)
{
	if (m >= n)
		return -1;
	double wynik = 0;
	double pom;
	for (int j = 0; j <= m; j++)
	{
		/*pom = wspC(n, j);
		pom = wspS(n, j);*/
		//wekA[j] = wspC(n, j) / wspS(n, j);
		wynik += (wspC(n, j) / wspS(n, j)) * Gram(j, n, q);
	}
	//cout << "wynik: " << wynik << endl;
	return wynik;

}
//funkcja obliczajaca blad sredniokwadratowy aproksymacji zbudowanej na wielomianach Grama
double bladA(int n, int m)
{
	if (n <= 0)
		return -1;
	double wynik = 0;
	for (int i = 0; i<n; i++)
	{
		//cout << funA(m, n, i) << " " << wekX[i] << " " << wekY[i] << endl;
		wynik += (funA(m, n, i) - wekY[i])*(funA(m, n, i) - wekY[i]);
	}
	wynik /= n-1  ;
	wynik = sqrt(wynik);
	return wynik;
}

void WyznaczWartosciAproks(int stopien)
{

	for (int j = 0; j < n;j++) {

		double wynik = 0;
		
			/*cout << funA(m, n, i) << " " << wekX[i] << " " << wekY[i] << endl;*/
			wynik = funA(stopien,n-1,j);
		
		cout<< wekX[j] << "\t\t"  << wekY[j] << "\t\t" <<
		 wynik << "\t\t" << endl;
		wekA[j] = wynik;
	}
	
}

void main()
{
	double tmp, min = INT_MAX;
	int pMin, sMin;
	double czasStart, czasStop;
	laduj(przedzial);
	cout << "Wartosc(x)\tWartosc(y)\t\tApprox\t\t " << endl;
	WyznaczWartosciAproks(stopien);
	cout << "Funkcja aproksymowana y=sin(x)*sin(x^2)-----------------------------------" << endl
		<< "Liczba wezlow = 50 -------------------------------------------------------\n"
		<< "--------------------------------------------------------------------------\n";
	cout << "| Stopien:\t| Przedzial:\t\t| Blad aproksymacji:\t\t |" << endl;
	czasStart = clock();
	
	
	for (int i = 1; i <= stopien; i++)
	{
	
		
		for (int j = 0; j<1; j++)
		{

			tmp = bladA(n, i);
			if (tmp<min) {
				min = tmp; sMin = i; pMin = przedzial + j;
			};
			cout << "| " << i << "\t\t| <" << -przedzial - j << "; " << przedzial + j << ">\t\t| ";
			cout.precision(10);
			cout.setf(ios::fixed, ios::floatfield);
			cout << tmp << "\t\t\t |\n";
		}
	}
	czasStop = clock();
	cout << "| Stopien:\t| Przedzial:\t\t| Blad aproksymacji:\t\t |" << endl
		<< "--------------------------------------------------------------------------\n";
	cout << "Funkcja aproksymowana y=sin(x)*sin(x^2)-----------------------------------" << endl
		<< "Liczba wezlow = 50 -------------------------------------------------------\n";
	cout << "Minimalny blad aproksymacji wynosi: " << min << ", dla stopnia: " << sMin << " oraz przedzialu: " << -pMin << "; " << pMin << ">\n"
		<< "Czas wykonywania sie programu: " << (long double)(czasStop - czasStart) / CLOCKS_PER_SEC << "[s]\n"
		<< "Srednio na jedno sprawdzenie: " << ((long double)(czasStop - czasStart) / CLOCKS_PER_SEC) / (5 * 20) << "[s]" << endl;

	getchar();
}


